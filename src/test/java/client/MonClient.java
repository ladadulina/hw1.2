package client;

import com.fasterxml.jackson.core.type.TypeReference;
import dto.CommentDto;
import lombok.SneakyThrows;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;

import java.util.List;

public class MonClient extends BaseClient {
    @SneakyThrows
    public ResponseWrapper<List<CommentDto>> getTheseComments(long userId) {
        HttpGet httpGet = new HttpGet(BASE_PATH + "/monitoring/api/comments?userId=" + userId);
        CloseableHttpResponse httpResponse = client.execute(httpGet);
        return new ResponseWrapper<>(httpResponse, new TypeReference<List<CommentDto>>() {
        });
    }
}
