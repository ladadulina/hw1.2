package client;

import com.fasterxml.jackson.core.type.TypeReference;
import dto.CommentDto;
import dto.OrderDto;
import dto.RegistrationDto;
import lombok.SneakyThrows;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.testng.collections.Lists;

import java.util.List;

public class UserClient extends BaseClient {

    @SneakyThrows
    public ResponseWrapper<RegistrationDto> register(RegistrationDto registrationDto) {
        HttpPost httpPost = new HttpPost(BASE_PATH + "/api/registration");
        httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
        httpPost.setEntity(new StringEntity(mapper.writeValueAsString(registrationDto)));

        CloseableHttpResponse httpResponse = client.execute(httpPost);
        return new ResponseWrapper<>(httpResponse, new TypeReference<RegistrationDto>() {
        });
    }

    @SneakyThrows
    public ResponseWrapper<String> login(String login, String password) {
        List<NameValuePair> pairs = Lists.newArrayList(new BasicNameValuePair("username", login),
                new BasicNameValuePair("password", password));

        HttpPost httpPost = new HttpPost(BASE_PATH + "/login");
        httpPost.setEntity(new UrlEncodedFormEntity(pairs));

        CloseableHttpResponse httpResponse = client.execute(httpPost);
        return new ResponseWrapper<>(httpResponse, new TypeReference<String>() {
        });
    }

    @SneakyThrows
    public ResponseWrapper<CommentDto> createComment(CommentDto comment, long itemId) {
        HttpPost httpPost = new HttpPost(BASE_PATH + "/api/user/item/" + itemId + "/comment");

        httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
        httpPost.setEntity(new StringEntity(mapper.writeValueAsString(comment)));

        CloseableHttpResponse httpResponse = client.execute(httpPost);
        return new ResponseWrapper<>(httpResponse, new TypeReference<CommentDto>() {
        });
    }

    @SneakyThrows
    public ResponseWrapper<CommentDto> updateComment(CommentDto commentDto) {
        HttpPut httpPut = new HttpPut(BASE_PATH + "/api/user/comment");
        httpPut.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
        httpPut.setEntity(new StringEntity(mapper.writeValueAsString(commentDto)));

        CloseableHttpResponse httpResponse = client.execute(httpPut);
        return new ResponseWrapper<>(httpResponse, new TypeReference<CommentDto>() {
        });
    }

    @SneakyThrows
    public ResponseWrapper<CommentDto> getComment(Long commId) {

        HttpGet httpGet = new HttpGet(BASE_PATH + "/api/comment/" + commId);

        CloseableHttpResponse httpResponse = client.execute(httpGet);
        return new ResponseWrapper<>(httpResponse, new TypeReference<CommentDto>() {
        });
    }


    @SneakyThrows
    public ResponseWrapper<String> deleteComment(Long commId) {
        HttpDelete httpDelete = new HttpDelete(BASE_PATH + "/api/user/comment/" + commId);

        CloseableHttpResponse httpResponse = client.execute(httpDelete);
        return new ResponseWrapper<>(httpResponse, new TypeReference<String>() {

        });
    }

    @SneakyThrows
    public ResponseWrapper<List<CommentDto>> getAllComments() {
        HttpGet httpGet = new HttpGet(BASE_PATH + "api/user/comments");

        CloseableHttpResponse httpResponse = client.execute(httpGet);
        return new ResponseWrapper<>(httpResponse, new TypeReference<List<CommentDto>>() {
        });
    }

    @SneakyThrows
    public ResponseWrapper<List<CommentDto>> getAllCommentsForItem(long itemId) {
        HttpGet httpGet = new HttpGet(BASE_PATH + "/admin/api/v2/shopItem/"+itemId+"/comments");

        CloseableHttpResponse httpResponse = client.execute(httpGet);
        return new ResponseWrapper<>(httpResponse, new TypeReference<List<CommentDto>>() {
        });
    }
    @SneakyThrows
    public ResponseWrapper<OrderDto> createOrder(OrderDto order) {
        HttpPost httpPost = new HttpPost(BASE_PATH + "/api/user/orderFast");
        httpPost.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
        httpPost.setEntity(new StringEntity(mapper.writeValueAsString(order)));

        CloseableHttpResponse httpResponse = client.execute(httpPost);
        return new ResponseWrapper<>(httpResponse, new TypeReference<OrderDto>() {
        });
    }

    @SneakyThrows
    public ResponseWrapper<CommentDto> updateOrder(OrderDto orderDto) {
        HttpPut httpPut = new HttpPut(BASE_PATH + "/admin/api/v2/order");
        httpPut.setHeader(new BasicHeader(HttpHeaders.CONTENT_TYPE, "application/json"));
        httpPut.setEntity(new StringEntity(mapper.writeValueAsString(orderDto)));

        CloseableHttpResponse httpResponse = client.execute(httpPut);
        return new ResponseWrapper<>(httpResponse, new TypeReference<CommentDto>() {
        });
    }

}


