package client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.ItemDto;
import lombok.SneakyThrows;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import java.util.List;

public class BaseClient {
    protected static final String BASE_PATH = "http://207.154.241.206:8080";
    protected CloseableHttpClient client = HttpClients.createDefault();
    protected ObjectMapper mapper = new ObjectMapper();

    @SneakyThrows
    public ResponseWrapper<List<ItemDto>> getItems() {
        HttpGet httpGet = new HttpGet(BASE_PATH + "/api/shopItems");

        CloseableHttpResponse httpResponse = client.execute(httpGet);
        return new ResponseWrapper<>(httpResponse, new TypeReference<List<ItemDto>>() {
        });
    }
}
