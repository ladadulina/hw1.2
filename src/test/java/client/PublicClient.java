package client;

import com.fasterxml.jackson.core.type.TypeReference;
import dto.ItemDto;
import lombok.SneakyThrows;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;

import java.util.List;

public class PublicClient extends BaseClient {
    @SneakyThrows
    public ResponseWrapper<List<ItemDto>> getItems() {
        HttpGet httpGet = new HttpGet(BASE_PATH + "/api/shopItems");

        CloseableHttpResponse httpResponse = client.execute(httpGet);
        return new ResponseWrapper<>(httpResponse, new TypeReference<List<ItemDto>>() {
        });
    }
}
