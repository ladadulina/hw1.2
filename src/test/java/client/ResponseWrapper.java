package client;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.http.Header;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;

import java.util.Arrays;
import java.util.List;

public class ResponseWrapper<T> {
    private final ObjectMapper mapper = new ObjectMapper();
    private int statusCode;
    private String content;
    private TypeReference<T> type;
    private List<Header> headers;

    @SneakyThrows
    public ResponseWrapper(CloseableHttpResponse response, TypeReference<T> type) {
        try {
            statusCode = response.getStatusLine().getStatusCode();
            this.type = type;
            content = EntityUtils.toString(response.getEntity());
            headers = Arrays.asList(response.getAllHeaders());
        } finally {
            EntityUtils.consume(response.getEntity());
        }
    }

    public ResponseWrapper<T> statusCodeIs(int statusCode) {
        Assert.assertEquals(this.statusCode, statusCode, "Wrong status code");
        return this;

    }

    @SneakyThrows
    public T parseContent() {
        return mapper.readValue(content, type);
    }
}
