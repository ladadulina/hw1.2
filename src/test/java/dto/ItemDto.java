package dto;
import lombok.Data;

@Data
public class ItemDto {
    private Long id;
    private Double price;
    private String itemName;
    private String itemDescription;
    private Long quantity;
    private Long categoryId;
}
