package mainHW;

import client.ResponseWrapper;
import client.UserClient;
import dto.CommentDto;
import dto.ItemDto;
import dto.RegistrationDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.SneakyThrows;
import org.testng.AssertJUnit;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.collections.Lists;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

import java.util.List;
import java.util.UUID;

@Data
public class HW2Test {

    private static final String BASE_PATH = "http://207.154.241.206:8080";
    private final ObjectMapper mapper = new ObjectMapper();
    private final String username = "Username" + System.currentTimeMillis();
    private final String pass = "pass" + System.currentTimeMillis();
    private final UserClient userClient = new UserClient();

    @SneakyThrows

    @BeforeClass
    public void setUp() {
        RegistrationDto registrationDto = RegistrationDto
                .builder()
                .username(username)
                .password(pass)
                .passwordConfirm(pass)
                .build();

        userClient
                .register(registrationDto)
                .statusCodeIs(200);

        ResponseWrapper<String> loginResponse = userClient
                .login(username, pass);

    }

    @Test(testName = "11. Complete registration and login")
    public void completeRegistrationLogin() {
        UserClient userClient = new UserClient();

        String username = "Username" + UUID.randomUUID();
        String pass = "pass" + System.currentTimeMillis();
        RegistrationDto registrationDto = RegistrationDto
                .builder()
                .username(username)
                .password(pass)
                .passwordConfirm(pass)
                .build();

        userClient
                .register(registrationDto)
                .statusCodeIs(200);

        ResponseWrapper<String> loginResponse = userClient
                .login(username, pass)
                .statusCodeIs(302);
    }

    @SneakyThrows
    @Test(testName = "12. Create a comment for item")
    public void createComment() {
        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        CommentDto commentDto = CommentDto
                .builder()
                .itemId(itemDto.getId())
                .comment("wow")
                .stars(3)
                .build();

        Long itemId = commentDto.getItemId();
        CommentDto commentResponseDto = userClient.createComment(commentDto, itemId).statusCodeIs(201).parseContent();
        assertNotNull(commentResponseDto.getId(), "id == null");
        assertEquals(commentResponseDto.getComment(), commentDto.getComment());
        assertEquals(commentResponseDto.getStars(), commentDto.getStars());

    }

    @SneakyThrows
    @Test(testName = "13. Validate comment field requirements(positive)", dataProvider = "ValidateComment")
    public void validateComment(String comments) {
        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        CommentDto commentDto = CommentDto
                .builder()
                .itemId(itemDto.getId())
                .comment(comments)
                .stars(4)
                .build();

        Long itemId = commentDto.getItemId();
        userClient.createComment(commentDto, itemId).statusCodeIs(201).parseContent();
    }

    @DataProvider
    public Object[] ValidateComment() {
        return Lists.newArrayList("excellent", "excellent comment").toArray();
    }

    @SneakyThrows
    @Test(testName = "13. Validate comment field requirements(negative)", dataProvider = "InValidComment")
    public void invalidComment(String comments) {
        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        CommentDto commentDto = CommentDto
                .builder()
                .itemId(itemDto.getId())
                .comment(comments)
                .stars(5)
                .build();

        Long itemId = commentDto.getItemId();
        userClient.createComment(commentDto, itemId).statusCodeIs(400).parseContent();

    }

    @DataProvider
    public Object[] InValidComment() {
        return Lists.newArrayList().toArray();
    }

    @SneakyThrows
    @Test(testName = "14. Validate itemId field requirements")
    public void validateItemIdFieldRequirements() {
        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        CommentDto commentDto = CommentDto
                .builder()
                .itemId(itemDto.getId())
                .comment("wow")
                .stars(3)
                .build();

        Long itemId = commentDto.getItemId();
        Long itemId2 = itemDto.getId();

        userClient.createComment(commentDto, itemId).statusCodeIs(201).parseContent();
        AssertJUnit.assertEquals(itemId, itemId2);

    }


    @SneakyThrows
    @Test(testName = "15. Validate stars field requirements(positive)", dataProvider = "ValidateStars")
    public void CanCreateCommentWthStars(int stars) {
        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        CommentDto commentDto = CommentDto
                .builder()
                .itemId(itemDto.getId())
                .comment("wow")
                .stars(stars)
                .build();

        Long itemId = commentDto.getItemId();
        userClient.createComment(commentDto, itemId).statusCodeIs(201).parseContent();

    }

    @DataProvider
    public Object[] ValidateStars() {
        return Lists.newArrayList(1, 5).toArray();
    }


    @SneakyThrows
    @Test(testName = "15. Validate stars field requirements(negative)", dataProvider = "InvalidStars")
    public void invalidStarsFieldRequirements(int stars) {
        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        CommentDto commentDto = CommentDto
                .builder()
                .itemId(itemDto.getId())
                .comment("wow")
                .stars(stars)
                .build();

        Long itemId = commentDto.getItemId();
        userClient.createComment(commentDto, itemId).statusCodeIs(400);
    }

    @DataProvider
    public Object[] InvalidStars() {

        return Lists.newArrayList(-3, 10).toArray();
    }

    @Test(testName = "16. Update comment")
    public void updateComment() {
        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        CommentDto commentDto = CommentDto
                .builder()
                .itemId(itemDto.getId())
                .comment("wow")
                .stars(3)
                .build();

        Long itemId = commentDto.getItemId();
        CommentDto commentResponseDto = userClient.createComment(commentDto, itemId).statusCodeIs(201).parseContent();

        commentResponseDto.setComment("some new comments");
        userClient.updateComment(commentResponseDto).statusCodeIs(200);
    }

    @Test(testName = "17. Get all user comments")
    public void getAllUserComments() {
        userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));

        userClient.getAllComments().statusCodeIs(200);

    }

    @Test(testName = "18. Delete comment")
    public void deleteComment() {

        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        CommentDto commentDto = CommentDto
                .builder()
                .itemId(itemDto.getId())
                .comment("wow")
                .stars(3)
                .build();

        Long itemId = commentDto.getItemId();
        CommentDto commentResponseDto = userClient.createComment(commentDto, itemId).statusCodeIs(201).parseContent();
        assertNotNull(commentResponseDto.getId(), "id == null");
        assertEquals(commentResponseDto.getComment(), commentDto.getComment());
        assertEquals(commentResponseDto.getStars(), commentDto.getStars());

        userClient.deleteComment(commentResponseDto.getId()).statusCodeIs(204);
    }

    @Test(testName = "19. Get comment by id")
    public void getCommentById() {
        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        CommentDto commentDto = CommentDto
                .builder()
                .itemId(itemDto.getId())
                .comment("wow")
                .stars(3)
                .build();
        Long commId = commentDto.getItemId();

        userClient.getComment(commId).statusCodeIs(200);
    }

    @Test(testName = "20. Get all comments for an item")
    public void getAllCommentsOfItem() {
        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        Long itemId = itemDto.getId();

        userClient.getAllCommentsForItem(itemId).statusCodeIs(200);
    }
}


