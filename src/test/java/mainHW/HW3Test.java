package mainHW;

import client.MonClient;
import client.PublicClient;
import client.UserClient;
import com.fasterxml.jackson.databind.ObjectMapper;
import dto.*;
import lombok.SneakyThrows;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.collections.Lists;

import java.util.List;


import java.util.concurrent.TimeUnit;

public class HW3Test {
    private ObjectMapper mapper = new ObjectMapper();
    private final String username = "Username" + System.currentTimeMillis();
    private final String pass = "pass" + System.currentTimeMillis();
    private final UserClient userClient = new UserClient();
    private PublicClient publicClient = new PublicClient();

    @BeforeClass
    public void setUp() {
        RegistrationDto registrationDto = RegistrationDto
                .builder()
                .username(username)
                .password(pass)
                .passwordConfirm(pass)
                .build();
        userClient
                .register(registrationDto)
                .statusCodeIs(200);
        userClient.login(username, pass);

    }

    @Test(testName = "21. Update new order as admin")
    public void updateNewOrderAsAdmin() {
        ItemDto itemDto = publicClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("Cannot find any items"));

        OrderDto orderDto = OrderDto
                .builder()
                .items(Lists.newArrayList(
                        QuantityDto
                                .builder()
                                .itemId(itemDto.getId())
                                .quantity(1L)
                                .build()))
                .build();

        Long id = orderDto.getId();
        OrderDto orderDto1 = userClient.createOrder(orderDto).statusCodeIs(200).parseContent();
        orderDto1.getItems().get(0).setQuantity(5L);

        userClient.updateOrder(orderDto1).statusCodeIs(200);
    }


    @SneakyThrows
    @Test(testName = "22. Create new comment and wait until it appears in monitoring service")
    public void createNewCommentAndWait() {
        ItemDto itemDto = userClient
                .getItems()
                .statusCodeIs(200)
                .parseContent()
                .stream()
                .findAny()
                .orElseThrow(() -> new AssertionError("No items found"));
        CommentDto commentDto = CommentDto
                .builder()
                .itemId(itemDto.getId())
                .comment("wow")
                .stars(3)
                .build();

        Long itemId = commentDto.getItemId();

        CommentDto commentResponseDto = userClient
                .createComment(commentDto, itemId)
                .statusCodeIs(201)
                .parseContent();


        MonClient monitoringClient = new MonClient();

        List<CommentDto> comments = monitoringClient
                .getTheseComments(userClient.register(RegistrationDto.builder().build()).parseContent().getId())
                .statusCodeIs(200)
                .parseContent();

        Long maxWaitTime = 9000L;
        Long startTime = System.currentTimeMillis();

        while (comments.stream().noneMatch(comment -> comment.getId().equals(userClient.register(RegistrationDto.builder().build()).parseContent().getId()))) {
            comments = monitoringClient
                    .getTheseComments(2)
                    .statusCodeIs(200)
                    .parseContent();

            TimeUnit.MILLISECONDS.sleep(110L);

            Long endTime = System.currentTimeMillis();

            Assert.assertTrue((endTime - startTime) < maxWaitTime, "Time is out");
        }
    }
}
