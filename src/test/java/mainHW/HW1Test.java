package mainHW;

import io.restassured.RestAssured;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.collections.Lists;

import java.util.Iterator;

import static org.hamcrest.Matchers.*;
import static io.restassured.RestAssured.given;

public class HW1Test {

    @BeforeClass
    public void setUp() {
        RestAssured.baseURI = "http://207.154.241.206:8080";
        RestAssured.filters(new RequestLoggingFilter(), new ResponseLoggingFilter());
    }


    @Test(testName = "1. Create an item without a category")
    public void createItemWithoutCategory() {
        String body = "{\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"7\",\n" +
                "        \"quantity\": \"0\"\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(201)
                .body("id", notNullValue());
    }

    @Test(testName = "2. Create an item with a category")
    public void createItemWithCategory() {
        String body = "{\n" +
                "        \"categoryId\": \"2\",\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"8\",\n" +
                "        \"quantity\": \"0\"\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(201)
                .body("id", notNullValue());
    }

    @Test(testName = "3. Validate itemDescription field requirements(positive)", dataProvider = "validateItemDescriptionField")
    public void validateItemDescriptionFieldRequirements(String itemDescript) {
        String body = "{\n" +
                "        \"categoryId\": \"2\",\n" +
                "        \"itemDescription\": \"" + itemDescript + "\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"8\",\n" +
                "        \"quantity\": \"0\"\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(201)
                .body("itemDescription", is(itemDescript));
    }

    @DataProvider
    public Object[] validateItemDescriptionField() {
        return Lists.newArrayList("Some", "Description").toArray();
    }

    @Test(testName = "3. Validate itemDescription field requirements(negative)", dataProvider = "incorrectItemDescriptionField")
    public void incorrectItemDescriptionFieldRequirements(String desc) {
        String body = "{\n" +
                "        \"categoryId\": \"2\",\n" +
                "        \"itemDescription\": \"" + desc + "\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"8\",\n" +
                "        \"quantity\": \"0\"\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(400);

    }

    @DataProvider
    public Object[] incorrectItemDescriptionField() {
        return Lists.newArrayList().toArray();
    }

    @Test(testName = "4. Validate itemName field requirements(positive)", dataProvider = "validateItemNameField")
    public void validateItemNameFieldRequirements(String name) {
        String body = "{\n" +
                "        \"categoryId\": \"6\",\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"" + name + "\",\n" +
                "        \"price\": \"8\",\n" +
                "        \"quantity\": \"0\"\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(201)
                .body("itemName", is(name));
    }

    @DataProvider
    public Object[] validateItemNameField() {
        return Lists.newArrayList("Some 4354", "Name").toArray();
    }

    @Test(testName = "4. Validate itemName field requirements(negative)", dataProvider = "invalidItemNameField")
    public void invalidItemNameFieldRequirements(String name) {
        String body = "{\n" +
                "        \"categoryId\": \"2\",\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"" + name + "\",\n" +
                "        \"price\":  \"8\",\n" +
                "        \"quantity\": \"0\"\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(400);
    }

    @DataProvider
    public Object[] invalidItemNameField() {
        return Lists.newArrayList().toArray();
    }

    @Test(testName = "5. Validate price field requirements(positive))", dataProvider = "validatePriceField")
    public void validatePriceFieldRequirements(float price1) {
        String body = "{\n" +
                "        \"categoryId\": \"2\",\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"" + price1 + "\",\n" +
                "        \"quantity\": \"0\"\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(201)
                .body("price", notNullValue());
    }

    @DataProvider
    public Object[] validatePriceField() {
        return Lists.newArrayList(65, 7687.76f).toArray();
    }


    @Test(testName = "5. Validate price field requirements(negative)", dataProvider = "invalidPriceField")
    public void invalidPriceFieldRequirements(float price1) {
        String body = "{\n" +
                "        \"categoryId\": \"2\",\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"" + price1 + "\",\n" +
                "        \"quantity\": \"2\",\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(400);

    }

    @DataProvider
    public Object[] invalidPriceField() {
        return Lists.newArrayList(-10340, -1).toArray();
    }

    @Test(testName = "6. Validate quantity field requirements(positive)", dataProvider = "validateQuantityField")
    public void validateQuantityFieldRequirements(int num) {
        String body = "{\n" +
                "        \"categoryId\": \"5\",\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"2\",\n" +
                "        \"quantity\": \"" + num + "\"\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(201)
                .body("quantity", notNullValue());
    }

    @DataProvider
    public Object[] validateQuantityField() {
        return Lists.newArrayList(0, 5342).toArray();
    }

    @Test(testName = "6. Validate quantity field requirements(negative)", dataProvider = "invalidQuantityField")
    public void invalidQuantityFieldRequirements(int num) {
        String body = "{\n" +
                "        \"categoryId\": \"5\",\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"2\",\n" +
                "        \"quantity\": \"" + num + "\"\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(400);
    }

    @DataProvider
    public Object[] invalidQuantityField() {
        return Lists.newArrayList(-1, -342).toArray();
    }

    @Test(testName = "7. Create an item and get it")
    public void creatItemGetIt() {
        String body = "{\n" +
                "        \"categoryId\": \"7\",\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"2\",\n" +
                "        \"quantity\": \"5\"\n" +
                "    }";
        long id = given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(201)
                .extract().response().jsonPath().getLong("id");
        given()
                .when()
                .get("/admin/api/v1/shopItem/" + id)
                .then()
                .statusCode(200)
                .body("id", equalTo((int) id));
    }

    @Test(testName = "8. Create an item and update it")
    public void creatItemUpdateIt() {
        String body = "{\n" +
                "        \"categoryId\": \"1\",\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"56\",\n" +
                "        \"quantity\": \"23\"\n" +
                "    }";
        long id = given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(201)
                .extract().response().jsonPath().getLong("id");
        String updatedBody = "{\n" +
                "        \"id\": \"" + id + "\",\n" +
                "        \"categoryId\": \"1\",\n" +
                "        \"itemDescription\": \"New description\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"459\",\n" +
                "        \"quantity\": \"23\"\n" +
                "    }";
        given()
                .when()
                .contentType(ContentType.JSON)
                .body(updatedBody)
                .put("/admin/api/v1/shopItem")
                .then()
                .statusCode(200);
        given()
                .when()
                .get("/admin/api/v1/shopItem/" + id)
                .then()
                .statusCode(200)
                .body("id", equalTo((int) id))
                .body("itemDescription", equalTo("New description"))
                .body("price", equalTo((float) 459));
    }

    @Test(testName = "9. Create an item and delete it")
    public void creatItemDeleteIt() {
        String body = "{\n" +
                "        \"categoryId\": \"3\",\n" +
                "        \"itemDescription\": \"string\",\n" +
                "        \"itemName\": \"string\",\n" +
                "        \"price\": \"29\",\n" +
                "        \"quantity\": \"1\"\n" +
                "    }";
        long id = given()
                .when()
                .contentType(ContentType.JSON)
                .body(body)
                .post("/admin/api/v1/shopItem")
                .then()
                .statusCode(201)
                .extract().response().jsonPath().getLong("id");
        given()
                .when()
                .delete("/admin/api/v1/shopItem/" + id)
                .then()
                .statusCode(204);
        given()
                .when()
                .delete("/admin/api/v1/shopItem/" + id)
                .then()
                .statusCode(404);
    }

    @Test(testName = "10. Get all items")
    public void getAllItems() {

        given()
                .when()
                .get("/admin/api/v1/shopItems")
                .then()
                .statusCode(200);
    }

}
